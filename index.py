from flask  import Flask,render_template,url_for


app = Flask(__name__)


@app.route('/')
def home():
    #return "Welcome to flask "
    return render_template('vue.html')

if __name__ == '__main__':
    app.run(debug=True)